﻿namespace 队列
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            Queue<int> queue = new();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Enqueue(5);
            queue.Enqueue(6);
            queue.Enqueue(7);
            queue.Enqueue(8);
            queue.Enqueue(9);
            queue.Enqueue(10);
            queue.Enqueue(11);

            //首个元素
            Console.WriteLine(queue.Peek().ToString());
            Console.WriteLine(queue.Count().ToString());

            Console.WriteLine(queue.Peek().ToString());
            queue.Dequeue();
            Console.WriteLine(queue.Count().ToString());
            Console.WriteLine(queue.Peek().ToString());
            queue.Dequeue();
            Console.WriteLine(queue.Count().ToString());
            Console.WriteLine(queue.Peek().ToString());
            queue.Dequeue();
            Console.WriteLine(queue.Count().ToString());
            Console.WriteLine(queue.Peek().ToString());
            queue.Dequeue();
            Console.WriteLine(queue.Count().ToString());
        }
    }
}
