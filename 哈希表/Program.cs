﻿namespace 哈希表
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /* 初始化哈希表 */
            Dictionary<int, string> map =
                new()
                {
                    /* 添加操作 */
                    // 在哈希表中添加键值对 (key, value)
                    { 12836, "小哈" },
                    { 15937, "小啰" },
                    { 16750, "小算" },
                    { 13276, "小法" },
                    { 10583, "小鸭" }
                };
            Console.WriteLine("Hello, World!");

            Console.WriteLine(map[16750].ToString());
            map.Add(16394, "askmlfa");
            foreach (var x in map)
            {
                Console.WriteLine(x.Key.ToString() + "->" + x.Value);
            }
        }
    }
}
