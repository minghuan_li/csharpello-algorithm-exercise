﻿namespace 二叉树
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            //初始化二叉树
            //初始化节点
            TreeNode n1 = new(1);
            TreeNode n2 = new(2);
            TreeNode n3 = new(3);
            TreeNode n4 = new(4);
            TreeNode n5 = new(5);
            n1.left = n2;
            n1.right = n3;
            n2.left = n4;
            n2.right = n5;

            //插入节点
            TreeNode P = new(0);
            n1.left= P;
            P.left = n2;
        }

        public class TreeNode
        {
            public int val;
            public  TreeNode? left;
            public  TreeNode? right;

            public TreeNode(int x)
            {
                val = x;
            }
        }
    }
}
