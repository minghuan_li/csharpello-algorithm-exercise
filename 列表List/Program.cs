﻿namespace 列表List
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            //无初始值
            List<int> list = new();
            //有初始值
            List<int> list2 = new List<int>() { 1,2,3,4,5};

            Console.WriteLine(list2[1].ToString());

            List<Student> list3 = new List<Student>();
            list3.Add(new Student() { Id=1,Name="xiaoming",Age=16});
            list3.Add(new Student() { Id = 1, Name = "xiaoming", Age = 16 });
            list3.Add(new Student() { Id = 2, Name = "xiaoming", Age = 16 });
            list3.Add(new Student() { Id = 3, Name = "xiaoming", Age = 16 });
            list3.Add(new Student() { Id = 4, Name = "xiaoming", Age = 16 });
            list3.Add(new Student() { Id = 5, Name = "xiaoming", Age = 16 });

            foreach(var item in list3)
            {
                Console.WriteLine(item.Id.ToString()+" "+item.Name.ToString() + " "+item.Age.ToString());
            }

            list3.Insert(2, new Student() { Id = 10, Name = "liminghuan" ,Age=23});
            Console.WriteLine("-----------------");
            foreach (var item in list3)
            {
                Console.WriteLine(item.Id.ToString() + " " + item.Name.ToString() + " " + item.Age.ToString());
            }
        }

        public class Student
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        }
    }
}