﻿namespace 栈
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            Stack<int> stack = new();
            //元素入栈
            stack.Push(1);
            stack.Push(3);
            stack.Push(2);
            stack.Push(5);
            stack.Push(4);

            //访问栈顶元素
            Console.WriteLine(stack.Peek().ToString());
            Console.WriteLine(stack.Count().ToString());

            //出栈
            stack.Pop();



        }
       
        
    }
}