﻿namespace 链表
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            ListNode n0 = new(1);
            ListNode n1 = new(3);
            ListNode n2 = new(2);
            ListNode n3 = new(5);
            ListNode n4 = new(4);
            // 构建引用指向
            n0.next = n1;
            n1.next = n2;
            n2.next = n3;
            n3.next = n4;
        }

        public class ListNode
        {
            public int val; //节点值
            public ListNode next; //指向下一节点的引用

            public ListNode(int x) => val = x; //构造函数
        }

        /* 初始化链表 1 -> 3 -> 2 -> 5 -> 4 */
        // 初始化各个节点
    }
}
